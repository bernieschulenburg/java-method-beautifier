/*
 * Copyright (C) 2015 Bernhard Schulenburg.
 *
 * This file is part of Java-method-beautifier.
 *
 * Java-method-beautifier can be used either under the terms of
 * a) the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * b) the terms of the Apache License
 *
 * You should have received a copy of both licenses in LICENCE.LGPL and
 * LICENCE.APACHE. Please refer to those files for details.
 *
 * Java-method-beautifier is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

package bschulenburg.method.beautifier;

/**
 * Created by bschulenburg on 20/10/15.
 */
public class Report {

  public static boolean verbose = false;

  public void reportVisit(String className,String currentMethodName,String changedMethodName){
    if(verbose)
      System.out.println("'"+className+"': found method ["+currentMethodName+"] -changed-> ["+changedMethodName+"].");
  }

  public void report(int OccurrencesFixed){
    System.out.println("");
    System.out.println(""+OccurrencesFixed+" occurrences (declarations and references) of misnamed methods were fixed. Files were written to folder [output].");
    System.out.println("");
  }

  public static void setVerbose(String[] args){
    if (args.length > 0) {
      if (args[0].equals("verbose")) {
        Report.verbose = true;
      }
    }
  }
}
