/*
 * Copyright (C) 2015 Bernhard Schulenburg.
 *
 * This file is part of Java-method-beautifier.
 *
 * Java-method-beautifier can be used either under the terms of
 * a) the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * b) the terms of the Apache License
 *
 * You should have received a copy of both licenses in LICENCE.LGPL and
 * LICENCE.APACHE. Please refer to those files for details.
 *
 * Java-method-beautifier is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

package bschulenburg.method.beautifier;

//bschulenburg.method.beautifier.BeautifierMain
public class BeautifierMain {

  public static void main(String[] args) throws Exception {
    Report.setVerbose(args);

    MethodFixer fixer = new MethodFixer();
    fixer.fixAllClasses();

    Output output = new Output();
    output.createFiles(fixer.getFiles(), fixer.getFileContents());

    Report report = new Report();
    report.report(fixer.getOccurrencesFixed());
  }
}
