/*
 * Copyright (C) 2015 Bernhard Schulenburg.
 *
 * This file is part of Java-method-beautifier.
 *
 * Java-method-beautifier can be used either under the terms of
 * a) the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * b) the terms of the Apache License
 *
 * You should have received a copy of both licenses in LICENCE.LGPL and
 * LICENCE.APACHE. Please refer to those files for details.
 *
 * Java-method-beautifier is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

package bschulenburg.method.beautifier;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by bschulenburg on 20/10/15.
 */
public class OccurrenceCounter {

  public int countOccurrences(String fileContent, String pattern){
    Pattern p = Pattern.compile(pattern);
    Matcher m = p.matcher(fileContent);
    int count = 0;
    while (m.find()){
      count += 1;
    }
    return count;
  }
}
