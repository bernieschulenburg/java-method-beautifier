/*
 * Copyright (C) 2015 Bernhard Schulenburg.
 *
 * This file is part of Java-method-beautifier.
 *
 * Java-method-beautifier can be used either under the terms of
 * a) the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * b) the terms of the Apache License
 *
 * You should have received a copy of both licenses in LICENCE.LGPL and
 * LICENCE.APACHE. Please refer to those files for details.
 *
 * Java-method-beautifier is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

package bschulenburg.method.beautifier;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by bschulenburg on 20/10/15.
 */
public class Input {

  /**
   * Adds all files in directoryName, and its sub folders to queue them up for method beautification
   */
  public void addFiles(String directoryName, ArrayList<File> files, ArrayList<String> FileContents) throws IOException {
    File directory = new File(directoryName);
    File[] fList = directory.listFiles();

    for (File file : fList) {
      if (file.isFile()) {
        if(file.getName().endsWith(".java")){
          
          files.add(file);
          FileContents.add( getFileContent(file) );

        }
      }
      else if (file.isDirectory()) {
        addFiles(file.getAbsolutePath(), files, FileContents);
      }
    }
  }

  private String getFileContent(File file)  throws IOException{
    String content = new Scanner(file).useDelimiter("\\Z").next();
    return content;
  }
}
