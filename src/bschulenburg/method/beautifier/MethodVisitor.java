/*
 * Copyright (C) 2015 Bernhard Schulenburg.
 *
 * This file is part of Java-method-beautifier.
 *
 * Java-method-beautifier can be used either under the terms of
 * a) the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * b) the terms of the Apache License
 *
 * You should have received a copy of both licenses in LICENCE.LGPL and
 * LICENCE.APACHE. Please refer to those files for details.
 *
 * Java-method-beautifier is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

package bschulenburg.method.beautifier;

import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.visitor.VoidVisitorAdapter;

/**
 * Created by bschulenburg on 19/10/15.
 */
public class MethodVisitor  extends VoidVisitorAdapter {

  private BaseMethodChangeConsumer ChangeConsumer;
  private String ClassName;

  public MethodVisitor(){

  }

  @Override
  public void visit(MethodDeclaration n, Object arg) {
    // here you can access the attributes of the method.
    // this method will be called for all methods in this
    // CompilationUnit, including inner class methods
    String currentMethodName = n.getName();
    String changedMethodName = changeFirstCharacterToLowerCase(currentMethodName);
    String className = this.ClassName;

    if(!currentMethodName.equals(changedMethodName)){
      new Report().reportVisit(className, currentMethodName, changedMethodName);
    }
    ChangeConsumer.methodNameChanged( currentMethodName, changedMethodName);
  }

  public void setChangeConsumer(BaseMethodChangeConsumer ChangeConsumer){
    this.ChangeConsumer = ChangeConsumer;
  }

  public void setClassName(String name){
    ClassName = name;
  }

  private String changeFirstCharacterToLowerCase(String s){
    if(Character.isUpperCase(s.charAt(0))){
      String firstLetter = s.substring(0,1);
      firstLetter = firstLetter.toLowerCase();
      s = s.substring(1);
      s = firstLetter + s;
    }
    return s;
  }
}
