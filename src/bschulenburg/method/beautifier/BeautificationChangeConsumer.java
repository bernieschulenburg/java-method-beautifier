/*
 * Copyright (C) 2015 Bernhard Schulenburg.
 *
 * This file is part of Java-method-beautifier.
 *
 * Java-method-beautifier can be used either under the terms of
 * a) the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * b) the terms of the Apache License
 *
 * You should have received a copy of both licenses in LICENCE.LGPL and
 * LICENCE.APACHE. Please refer to those files for details.
 *
 * Java-method-beautifier is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

package bschulenburg.method.beautifier;

import java.util.ArrayList;

/**
 * Created by bschulenburg on 20/10/15.
 */
public class BeautificationChangeConsumer extends BaseMethodChangeConsumer{

  private ArrayList<String> FileContents;
  private int OccurrencesFixed;

  private void replaceReferencesInAllFiles(String oldName, String newName){

    for(int i = 0; i < FileContents.size(); i++){

      String fileContent = FileContents.get(i);

      if( oldName.equals(newName)) continue;

      //count standalone-word occurrences of oldName
      OccurrencesFixed += new OccurrenceCounter().countOccurrences(fileContent, "\\b" + oldName + "\\b");

      //regexp: replace any standalone-word occurrence of oldName, with newName
      fileContent = fileContent.replaceAll("\\b" + oldName + "\\b", newName);

      FileContents.remove(i);
      FileContents.add(i, fileContent);
    }
  }

  @Override
  public void methodNameChanged(String oldName, String newName) {
    try{
      replaceReferencesInAllFiles(oldName, newName);
    }
    catch(Exception e){
      e.printStackTrace();
    }
  }

  public void setFileContents(ArrayList<String> fileContents) {
    FileContents = fileContents;
  }

  public int getOccurrencesFixed() {
    return OccurrencesFixed;
  }

  public void setOccurrencesFixed(int occurrencesFixed) {
    OccurrencesFixed = occurrencesFixed;
  }

}
