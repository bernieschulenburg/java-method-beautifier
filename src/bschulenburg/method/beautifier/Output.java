/*
 * Copyright (C) 2015 Bernhard Schulenburg.
 *
 * This file is part of Java-method-beautifier.
 *
 * Java-method-beautifier can be used either under the terms of
 * a) the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * b) the terms of the Apache License
 *
 * You should have received a copy of both licenses in LICENCE.LGPL and
 * LICENCE.APACHE. Please refer to those files for details.
 *
 * Java-method-beautifier is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

package bschulenburg.method.beautifier;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by bschulenburg on 20/10/15.
 */
public class Output {

  //output
  public void createFiles(ArrayList<File> files, ArrayList<String> FileContents) throws Exception{
    String root = System.getProperty("user.dir");
    for(int i = 0; i < files.size(); i++){

      File file = files.get(i);

      String canonic = file.getCanonicalPath();
      String inputFolder = "/input/";
      String relativePath  = canonic.substring(root.length() + inputFolder.length());
      String outputPath ="output/"+relativePath;
      String onlyPathNotFile= outputPath.substring(0, outputPath.lastIndexOf("/"));

      makeDirs(onlyPathNotFile);

      PrintWriter writer = new PrintWriter(outputPath, "UTF-8");
      writer.println(FileContents.get(i));
      writer.close();

    }
  }

  //output
  private boolean makeDirs(String path){
    File f = new File(path);
    if (!f.isDirectory()) {
      boolean success = f.mkdirs();
      if (success) {
        //Created path
        return true;
      } else {
        //Could not create path
        return false;
      }
    } else {
      //Path already exists
      return true;
    }
  }
}
