/*
 * Copyright (C) 2015 Bernhard Schulenburg.
 *
 * This file is part of Java-method-beautifier.
 *
 * Java-method-beautifier can be used either under the terms of
 * a) the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * b) the terms of the Apache License
 *
 * You should have received a copy of both licenses in LICENCE.LGPL and
 * LICENCE.APACHE. Please refer to those files for details.
 *
 * Java-method-beautifier is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

package bschulenburg.method.beautifier;

import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by bschulenburg on 19/10/15.
 */
public class MethodFixer {

  private ArrayList<File> files;
  private ArrayList<String> FileContents;
  private int OccurrencesFixed;
  private BeautificationChangeConsumer consumer;

  public MethodFixer(){
    OccurrencesFixed = 0;
    initiateInputFiles();
  }

  private void initiateInputFiles(){
    files = new  ArrayList<File> ();
    FileContents = new ArrayList<String> ();
    //load files and their content into files and FileContent
    try{
      new Input().addFiles("input", files, FileContents);
    }
    catch(IOException e){
      e.printStackTrace();
    }
  }

  private void fixClass(File file, int fileIndex) throws IOException, ParseException {
    // creates an input stream for the file to be parsed
    FileInputStream in = new FileInputStream(file);
    CompilationUnit cu;
    try {
      // parse the file
      cu = JavaParser.parse(in);
    } finally {
      in.close();
    }
    // visit and print the methods names
    MethodVisitor visitor = new MethodVisitor();
    BeautificationChangeConsumer consumer = inititateChangeConsumer();
    visitor.setChangeConsumer(consumer);
    String className = files.get(fileIndex).getName();
    visitor.setClassName(className);
    visitor.visit(cu, null);
    this.OccurrencesFixed = consumer.getOccurrencesFixed();
  }

  private BeautificationChangeConsumer inititateChangeConsumer(){
    BeautificationChangeConsumer consumer = new BeautificationChangeConsumer();
    consumer.setFileContents(this.FileContents);
    consumer.setOccurrencesFixed(this.OccurrencesFixed);
    return consumer;
  }

  public void fixAllClasses() throws IOException, ParseException{
    for(int i =0; i < files.size(); i++){
      File file = files.get(i);
      fixClass(file, i);
    }
  }

  public ArrayList<File> getFiles(){
    return files;
  }

  public ArrayList<String> getFileContents(){
    return FileContents;
  }

  public int getOccurrencesFixed(){
    return OccurrencesFixed;
  }
}
