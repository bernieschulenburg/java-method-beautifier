# java-method-beautifier #

![logo.jpg](https://bitbucket.org/repo/ggzRBE/images/3441369320-logo.jpg)

### What does it do? ###

* `java-method-beautifier` beautifies method names in java source code. 

* *Beautification* here means changing it from 'MethodName' to 'methodName' lower-casing the first letter in the name. If the method name is already *beautiful* `java-method-beautifier` will leave the name as is.

* Method declarations, and all references to that method are adjusted to match the new name. The result should be compilable java code which does the same thing as the old code.

* The main reason this tool could be useful to you is if -like me- you have bad coding style, in the way that you end up with some of the method names starting with uppercase letters and others with lowercase, for no particular reason, other than sloppiness. In that sense the tool could also be named: `method-name-unsloppifier`

### How do I use it? ###

* You can use the [executable jar](https://bitbucket.org/bernieschulenburg/java-method-beautifier/downloads) as a command line tool.

* `java-method-beautifier.jar` lives in the same folder as `input`.

* Place your java project (all your sources -as they are- with sub folders and all) under `input`.

* When you run the jar, a folder `output` will be created, which should contain your project with all methods beautified.

* To run: 

> `java -jar java-method-beautifier.jar`

* You can also opt for a more verbose report:

> `java -jar java-method-beautifier.jar verbose`

### Additional notes ###

* Caveat: If you have method names with a capitalised prefix, example: `DOMparse()`, then java-method-beautifier will change it to `dOMparse()`, which may not be what you intended.

* Caveat 2: if your java code contains generics (such as `List<MyType>`) make sure that it instantiates like so `new List<MyType>()`, as opposed to `new List<>()`. The latter code will cause a `japa.parser.ParseException`.

* Licensing: `java-method-beautifier` is available either under the terms of the LGPL License or the Apache License. You as the user are entitled to choose the terms under which to adopt `java-method-beautifier`. For the sources and the licensing options go [here](https://bitbucket.org/bernieschulenburg/java-method-beautifier/src/bb69e036887e8fb081fe3c6e680bc06a2b7f0ed0?at=master)

* `java-method-beautifier` is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

* Version 0.1